const bs = require('browser-sync').create('Metalsmith')
const config = require('./config.js')
const debug = require('debug')('Run')
const metalsmith = require('./metalsmith')
const path = require('path')
const strip = require('strip-ansi')

function build (sync) {
  debug('Building Metalsmith')
  metalsmith.build((err) => {
    if (err) {
      debug('Metalsmith build error:')
      debug(err)
      if (sync) {
        return bs.sockets.emit('fullscreen:message', {
          title: 'Metalsmith Error:',
          body: strip(`${err.message}\n\n${err.stack}`),
          timeout: 100000
        })
      } else {
        throw err
      }
    }
    debug('Metalsmith build finished!')
    if (sync) {
      bs.reload()
    }
  })
}

function serve () {
  bs.init({
    server: config.paths.serverRoot,
    port: 8080,
    ui: {
      port: 9000
    },
    open: false,
    logLevel: 'debug',
    logPrefix: 'BrowserSync',
    logConnections: true,
    logFileChanges: true,
    notify: true,
    files: [{
      match: [
        path.resolve(config.paths.projectRoot, 'content', '**', '*'),
        path.resolve(config.paths.projectRoot, 'layouts', '**', '*.njk')
      ],
      fn: function (event, file) {
        build(true)
      },
      options: {
        ignored: ['**/.#*', '**/*~', '**/#*#']
        // /\.#|node_modules|~$/
      }
    }]
  })
}

const args = process.argv.slice(2)

switch (args[0]) {
  case 'build':
    build()
    break
  case 'serve':
    serve()
    break
  default:
    console.log('Unknown arguments "' + args[0] + '"')
}
