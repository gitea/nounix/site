/* This is the actual metalsmith configuration script. */
const assets = require('metalsmith-assets')
const cleanCSS = require('metalsmith-clean-css')
const config = require('./config.js')
const collections = require('@metalsmith/collections')
const excerpts = require('@metalsmith/excerpts')
const groff = require('metalsmith-groff')
const layouts = require('@metalsmith/layouts')
const Metalsmith = require('metalsmith')
const markdown = require('metalsmith-markdownit')
const moment = require('moment')
const permalinks = require('@metalsmith/permalinks')
const rename = require('metalsmith-rename')
const sitemap = require('metalsmith-sitemap')
const slug = require('slug')
const statistics = require('./metalsmith-statistics-plugin')

const __PROD__ = process.env.NODE_ENV === 'production'

moment.locale('fr')

module.exports = new Metalsmith(config.paths.projectRoot)
  .clean(__PROD__)
  .metadata({
    moment: moment,
    production: __PROD__,
    remove_diatrics: function (e) { return slug(e, { mode: 'rfc3986' }) },
    site: {
      title: 'Ti Nuage'
    }
  })
  .source(config.paths.metalsmithSource)
  .destination(config.paths.metalsmithDestination)
  .use(cleanCSS({
    files: 'content/css/*.css',
    cleanCSS: {
      rebase: true
    }
  }))
  .use(assets({
    source: './assets/' + (process.env.NODE_ENV || 'dev'),
    destination: './'
  }))
  .use(groff())
  .use(markdown({
    html: true,
    linkify: true,
    typographer: true,
    quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    plugin: {
      pattern: '**/*.md',
      extension: 'html'
    }
  }))
  .use(excerpts())
  .use(permalinks({
    pattern: 'blog/:date/:title',
    date: 'YYYY/MM'
  }))
  .use(collections({
    archives: {
      pattern: 'blog/*/**/*/*.html',
      reverse: true,
      sortBy: 'date'
    },
    news: {
      limit: 3,
      pattern: 'blog/*/**/*.html',
      reverse: true,
      sortBy: 'date'
    }
  }))
  .use(layouts({
    default: 'article.njk',
    pattern: '**/*.html',
    engineOptions: {
      filters: {
      },
      globals: {
        production: __PROD__
      }
    }
  }))
  .use(rename([
    [/\.njk$/, '.html']
  ]))
  .use(sitemap({
    changefreq: 'monthly',
    hostname: config.hostname
  }))
  .use(statistics())
