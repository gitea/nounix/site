# Code source du site internet de Ti Nuage

L'association Ti Nuage offre une palette des services numériques pour ses adhérents et les internautes, et promeut les logiciels libres et un usage éclairé et raisonné d'Internet.
Le site de l'association doit décrire ses missions et fournir un aperçu des services qu'elle propose, ainsi que de leurs conditions d'usage.

## Technologie

Le site est créé à l'aide du logiciel libre [MetalSmith](http://www.metalsmith.io). Il s'agit d'un outil de tansformation de fichiers qui permet en particulier la génération de sites statiques. L'intérêt de tels sites est leur facilité de déploiement, le peu d'usage qu'ils font en terme de ressources et leur robustesse. L'inconvénient est la nécessité de connaître quelques rudiments sur l'usage d'une ligne de comande pour re-générer et re-déployer le site à chaque modification.

## Sources

**Metalsmith** requiert la disponibilité de **NodeJS** et **npm**.

Pour installer les sources du site sur votre machine :

    git clone https://apps.ti-nuage.fr/gitea/ti-nuage/site.git

Le projet s'appuie sur un certain nombre de dépendances, utiles au developpement ou à la génération du site lui-même. Pour installer ces dépendances :

    npm install

## Commandes

Le script `package.json` comprend une série de commandes utiles au développement et à la génération du site. Les plus utiles sont :

- pour générer le site : `npm run build`,

- pour le servir localement et le tester sur son navigateur : `npm run server`.

La liste des dommandes existantes est disponible avec `npm run`.
