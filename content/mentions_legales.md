---
permalink: false
title: Mentions légales
---
## Nous contacter

L'adresse électronique de contact de l'association est : contact @ ti-nuage.fr. Le courrier doit être adressé au siège social.

Il est possible de nous rencontrer tous les premiers jeudi du mois à partir de 20 heures à [la Hutte](https://lahutte.ti-nuage.fr/), l'espace de travail partagé à Vieux Marché.

## Publication
Directeur de publication : Nourdine Gernelle, président de l'association.

**Ti Nuage** est une association loi 1901 déclarée à la préfecture de Lannion le 18 octobre 2021.
<br>RNA : `W223005884`
<br>SIREN : `904 949 534 00013`
<br>Siège social : `Mairie, 11 places aux Chevaux, 22420 Le Vieux Marché - France`

## Hébergement des données

Ce site et toutes les données de l'association sont actuellement stockées chez la Sas OVH.
<br>Siège social : `2 rue Kellermann, 59100 Roubaix - France`

## Propriété intellectuelle

Ce site est conçu avec le logiciel libre [Metalsmith](https://www.metalsmith.io). Les sources sont disponibles sur notre [forge logicielle](https://apps.ti-nuage.fr/gitea/ti-nuage/site).

Sauf mention contraire sur les supports utilisés, tout le contenu affiché sur le site [ti-nuage.fr](/) appartient à l'association **Ti Nuage** et est distribuable sous licence [CC-BY-SA version 2.0 ou ultérieure](https://creativecommons.org/licenses/by-sa/2.0/fr).

Les icones utilisés pour la description des services sont réalisés avec **Uicons** de [Flaticon](https://www.flaticon.com/uicons). L'icône utilisée pour marquer les liens externes fait partie du jeu d'icône [Feather](https://feathericons.com) et est distribué sous les termes de [la licence MIT](https://mit-license.org).

