---
permalink: false
title: Les services fournis par Ti Nuage
---
Les serveurs fournissant les services ci-dessous sont sous le seul contrôle de
l'équipe technique de l'association.

Nous sommes une petite structure et nous nous efforçons de donner le meilleur de nous même avec les moyens que nous avons à notre disposition. Malgré des sauvegardes quotidiennes et un suivi régulier de l'infrastructure, nous ne sommes pas à l'abris d'un incident. Nous ne garantissons donc pas une continuité de service permanente et nous ne nous assignons aucune obligation de résultat.

<section class="Services">
  <h2 class="tac mt128 mb64">Services réservés aux adhérents</h2>
  <div class="Two-cols-Two-rows">
    <div>
      <h3 class="services-title services-icon-web">Hébergement web</h3>
      <p>Nous ne le créons pas pour vous, mais nous pouvons vous apporter des conseils et nous hébergeons votre site internet. Vous pouvez utiliser votre propre noms de domaine où utiliser gratuitement un sous-domaine ti-nuage.fr</p>
    </div>
    <div>
      <h3 class="services-title services-icon-cloud">Drive</h3>
      <p>Partagez et collaborez sur des documents, gérez vos tâches et votre calendrier, synchronisez les contacts de votre téléphone, et bien plus encore.</p>
      <p>Logiciel <a href="https://nextcloud.com/">NextCloud</a>.</p>
    </div>
    <div>
      <h3 class="services-title services-icon-email">Adresse e-mail</h3>
      <p>Une adresse email @ti-nuage.fr consultable par webmail ou avec votre client de messagerie habituel.</p>
    </div>
    <div>
      <h3 class="services-title services-icon-ml">Liste de diffusion</h3>
      <p>Une liste de diffusion nom@listes.ti-nuage.fr pour échanger avec les membres de votre groupe de travail et collaborateurs.</p>
      <p>Addministration des listes avec le logiciel <a href="https://www.sympa.org/">Sympa</a>.
    </div>
    <div>
      <h3 class="services-title services-icon-key">Coffre-fort numérique</h3>
      <p>Un espace pour stocker vos notes et mots de passe de façon sécurisée et pouvoir y accéder depuis tous vos terminaux numériques.</p>
      <p>Logiciel <a href="https://bitwarden.com/">Bitwarden</a>.</p>
    </div>
    <div>
      <h3 class="services-title services-icon-gear">Forge logicielle</h3>
      <p>Un environnement de développement Git pour héberger et partager vos projets.</p>
      <p>Logiciel <a href="https://gitea.io/en-us/">Gitea</a>.</p>
    </div>
    <div>
      <h3 class="services-title services-icon-messages">Discussion instantanée</h3>
      <p>Une adresse sur le réseau de discussion <a href="https://fr.wikipedia.org/wiki/Fediverse">fédéré</a> XMPP.</p>
      <p>Logiciel <a href="https://apps.ti-nuage.fr/converse/">Converse</a> sur votre navigateur, mais aussi <a href="https://conversations.im/">Conversations</a>, <a href="https://dino.im/">Dino</a>, <a href="https://gajim.org/">Gajim</a> ...</p>
    </div>
  </div>
</section>

<section class="Services">
  <h2 class="tac mt128 mb64">Services en libre accès, sans adhésion</h2>
  <div class="Two-cols-Two-rows">
    <div>
      <h3 class="services-title services-icon-edit">Édition collaborative</h3>
      <p>Des outils pour organiser un document et le rédiger en groupe, avec la possibilité d'identifier les apports de chacun et de revenir en arrière.<p>
      <p>Logiciel <a href="https://apps.ti-nuage.fr/pad">Etherpad</a>, pour l'édition d'un texte.</p>
      <p>Logiciel <a href="https://calc.ti-nuage.fr">Ethercalc</a>, pour la création d'une feuille de calcul.</p>
    </div>
    <div>
      <h3 class="services-title services-icon-list">Sondages</h3>
      <p>Pour organiser des rendez-vous ou choisir parmi plusieurs options, au sein d'un groupe ou d'une association.</p>
      <p>Logiciel <a href="https://apps.ti-nuage.fr/date">OpenSondage</a></p>
    </div>
    <div>
      <h3 class="services-title services-icon-share">Partage de fichiers</h3>
      <p>Pour partager des documents trop lourds pour un envoi par courriel, une gallerie de photos, ou même de simple extraits de texte ou de code, de manière sécurisée et temporaire.</p>
      <p>Logiciel <a href="https://apps.ti-nuage.fr/privatebin">PrivateBin</a>, pour le partage d'extraits de textes.</p>
      <p>Logiciel <a href="https://send.ti-nuage.fr/">Send</a>, pour l'envoi de fichiers.</p>
    </div>
  </div>
</section>
