---
title: Réunion du mois de juin
date: 2022-06-12
author: David Soulayrol
---
La prochaine réunion de l'association se tiendra le **mardi 12 juin** à 20 heures, à la mairie de Vieux-Marché. (Nous étudions la possibilité de nous réunir dorénavant au bar de la place, à deux pas de la mairie, pour davantage de convivialité. Le site sera mis à jour en cas de changement.)

Cette réunion est l'occasion pour les adhérents de se rencontrer, de discuter de leurs besoins, de partager leurs connaissances. Elle est le lieu ou les membres du bureau ou les administrateurs peuvent expliquer leurs dernières actions. Elle est également l'occasion pour les personnes non inscrites à l'association de découvrir ses activités et de questionner ses membres ; elle est donc ouverte à tous.

Ce mois-ci, le bureau discutera en particulier ;

- de [l'appel à projets numériques](https://cotesdarmor.fr/numerique) émis par le département, de l'opportunité d'y répondre et des actions à mener pour cela ;
- des actions restant à réaliser pour proposer notre candidature à la prochaine portée des [CHATONS](https://www.chatons.org) ;
- de la pertinence et des risques juridiques liés à l'outil de partage anonyme de fichiers ;
- du choix de la solution de messagerie instantanée destinée éventuellement à remplacer **Mattermost** ;
- de la possibilité de participer au forum aux associations à la rentrée.

