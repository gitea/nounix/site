module.exports = {
  extends: 'stylelint-config-standard',
  rules: {
    'selector-nested-pattern': /^&[^,\n\r]+(,\s*&[^,\n\r]+)*$/,
    'selector-type-no-unknown': null
  },
  ignoreFiles: ['**/mini-tes.css']
}
